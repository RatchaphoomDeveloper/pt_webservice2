﻿using Model.AppConfig;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Data.SqlClient;

namespace DBConnection
{
    public class ORAConnection
    {
        private static ORAConnection _instance = null;
        public static ORAConnection Ins
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ORAConnection();
                }
                return _instance;
            }
        }

        public SqlConnection DBConnection
        {
            get
            {
                try
                {
                    SqlConnection oraConn = null;
                    string connStr = AppSettings.Ins.GetOracleConnStr();
                    oraConn = new SqlConnection(connStr);

                    return oraConn;
                }
                catch (Exception ex)
                {
                    throw new Exception("ORAConnection/DBConnection: " + ex.Message);
                }
            }
        }

        public void DBCloseConnect(SqlConnection oraConn)
        {
            try
            {

                if (oraConn != null)
                {
                    if (oraConn.State == ConnectionState.Open)
                    {
                        oraConn.Close();
                    }
                }

            }
            catch (Exception)
            {
            }
        }
    }
}
