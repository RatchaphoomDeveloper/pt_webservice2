﻿using Management;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Model;
using Model.AppConfig;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PT_WEBSERVICE.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        [AllowAnonymous]
        [HttpPost]
        [Route("api/UserLogin")]
        public IActionResult UserLogin([FromBody] UserModel data)
        {
            var output = new OutputModel();
            try
            {
                output = UserManagement.Ins.UserLoginMng(data);
                if(output.Status == SystemStatus.SUCCESS)
                {
                    return Ok(output.Data);
                }
                return Ok(output.Data);
            }
            catch (Exception ex)
            {
                output.Message = "UserLogin: " + ex.Message;
                return BadRequest(new { Message = output.Message + ""});
            }
        }
    }
}
