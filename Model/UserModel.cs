﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//#                        _oo0oo_
//#                       o8888888o
//#                       88" . "88
//#                       (| -_- |)
//#                       0\  =  /0
//#                     ___/`---'\___
//#                   .' \|     |// '.
//#                  / \|||  :  |||// \
//#                 / _||||| -:- |||||- \
//#                |   | \\  -  /// |   |
//#                | \_|  ''\---/''  |_/ |
//#                \  .-\__  '-'  ___/-. /
//#              ___'. .'  /--.--\  `. .'___
//#           ."" '<  `.___\_<|>_/___.' >' "".
//#          | | :  `- \`.;`\ _ /`;.`/ - ` : | |
//#          \  \ `_.   \_ __\ /__ _/   .-` /  /
//#      =====`-.____`.___ \_____/___.-`___.-'=====
//#                        `=---='


namespace Model
{
    // input
    public class UserModel
    {
        public string M_USERNAME { get; set; }
        public string M_PASSWORD { get; set; }
        public string M_NAME { get; set; }
        public string M_LASTNAME { get; set; }
    }

    // object array output
    public class UserResponseModel
    {
        public List<UserData> userDatas { get; set; }
    }

    // single object output
    public class UserData
    {
        [JsonProperty("ID")]
        public string ID { get; set; }
        [JsonProperty("TOKEN")]
        public string TOKEN { get; set; }
        [JsonProperty("M_USERNAME")]
        public string M_USERNAME { get; set; }
        [JsonProperty("M_NAME")]
        public string M_NAME { get; set; }
        [JsonProperty("M_LASTNAME")]
        public string M_LASTNAME { get; set; }
        [JsonProperty("CREATE_DATE")]
        public string CREATE_DATE { get; set; }
        [JsonProperty("UPDATE_DATE")]
        public string UPDATE_DATE { get; set; }
    }

}
