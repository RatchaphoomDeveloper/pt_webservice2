﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.AppConfig
{
    public class Messenger
    {
        private static Messenger _instance = null;
        public static Messenger Ins
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Messenger();
                }
                return _instance;
            }
        }

        public string GetMessage(int rescode,string message = "") {
            try
            {
                if (rescode == SystemStatus.SUCCESS) return "ทำรายการสำเร็จ";
                if (rescode == SystemStatus.ERROR) return "ทำรายการไม่สำเร็จ";
                if (rescode == SystemStatus.DATA_NOT_FOUND) return "ไม่พบข้อมูล";
                if (rescode == SystemStatus.INVALID_USER) return "ชื่อผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง";
                return "กรุณาติดต่อผู้ดูแลระบบ";
            }
            catch (Exception ex)
            {
                throw new Exception("GetMessage: "+ex.Message);
            }
        
        }
    }
}
