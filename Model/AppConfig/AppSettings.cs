﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Model.AppConfig
{
    public class AppSettings
    {
        private static AppSettings _instance = null;
        public static AppSettings Ins
        {
            get
            {
                if(_instance == null)
                {
                    _instance = new AppSettings();
                }
                return _instance;
            }
        }

        public IConfigurationRoot GetConfigData()
        {
            try
            {
                var builder = new ConfigurationBuilder();
                builder.SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json",optional:true,reloadOnChange : true);
                return builder.Build();
            }
            catch (Exception)
            {

                return null;
            }
        }

        public string GetOracleSchema()
        {
            try
            {
                var configuration = GetConfigData();
                string schema = configuration.GetSection("OracleSchema").Value;

                return schema;
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }

        public string GetOracleConnStr()
        {
            try
            {
                var configuration = GetConfigData();
                string connStr = configuration.GetSection("OracleConnStr").Value;

                return connStr;
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }

        public string GenerateToken(string user)
        {
            try
            {
                var configuration = GetConfigData();
                string key = configuration.GetSection("Authentication").GetSection("Key").Value;
                string issuer = configuration.GetSection("Authentication").GetSection("Issuer").Value;

                var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
                var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

                var claims = new[]
                {
                    new Claim(JwtRegisteredClaimNames.Sub,user),
                    new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString())
                };

                var jwtToken = new JwtSecurityToken(
                    issuer: issuer,
                    audience: issuer,
                    claims,
                    expires: DateTime.Now.AddHours(8),
                    signingCredentials: credentials
                    );

                string token = new JwtSecurityTokenHandler().WriteToken(jwtToken);

                return token;

            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }
    }
}
