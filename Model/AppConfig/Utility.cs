﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.AppConfig
{
    public class Utility
    {
        private static Utility _instance = null;
        public static Utility Ins
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Utility();
                }
                return _instance;
            }
        }

        public JsonSerializerSettings GetJsonSettings()
        {
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                DateFormatString = "dd/MM/yyyy HH:mm:ss",
                Formatting = Newtonsoft.Json.Formatting.Indented
            };

            return settings;
        }
    }
}
