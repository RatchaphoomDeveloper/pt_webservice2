﻿using Model;
using Model.AppConfig;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Businesslogic
{
    public class UserLogic
    {
        string schema = AppSettings.Ins.GetOracleSchema();
        private static UserLogic _instance = null;
        public static UserLogic Ins
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new UserLogic();
                }
                return _instance;
            }
        }

        public int UserLoginLogic(SqlConnection oraConn, UserModel loginData, out UserResponseModel data)
        {
            try
            {
                DataSet ds;
                int rescode;
                data = null;

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = oraConn;
                cmd.CommandText = "DEV_FOR_FLUKE_M_USERS_LOGIN";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 60;

                cmd.Parameters.Add("@M_USERNAME", SqlDbType.NVarChar).Value = loginData.M_USERNAME;
                cmd.Parameters.Add("@M_PASSWORD", SqlDbType.NVarChar).Value = loginData.M_PASSWORD;
                cmd.Parameters.Add("@RETURN_CODE", SqlDbType.Int).Direction = ParameterDirection.Output;

                ds = new DataSet();
                using(SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds);
                }

                if(ds.Tables[0].Rows.Count > 0)
                {
                    rescode = Convert.ToInt32(cmd.Parameters["@RETURN_CODE"].Value.ToString());
                    if(rescode == SystemStatus.SUCCESS)
                    {
                        ds.Tables[0].TableName = "userDatas";
                        var jsonData = JsonConvert.SerializeObject(ds, Utility.Ins.GetJsonSettings());
                        data = JsonConvert.DeserializeObject<UserResponseModel>(jsonData);
                    }
                }
                else
                {
                    rescode = SystemStatus.INVALID_USER;
                }

                return rescode;

            }
            catch (Exception ex)
            {

                throw new Exception("UserLoginLogic: " + ex.Message);
            }
        }
    }
}
