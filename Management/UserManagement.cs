﻿using Businesslogic;
using DBConnection;
using Model;
using Model.AppConfig;
using System;
using System.Data.SqlClient;

namespace Management
{
    public class UserManagement
    {
        OutputModel output;
        SqlConnection oraConn;
        SqlTransaction trans;
        private static UserManagement _instance = null;
        public static UserManagement Ins
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new UserManagement();
                }
                return _instance;
            }
        }

        public OutputModel UserLoginMng(UserModel user) {
            try
            {
                string message = "";
                int rescode = SystemStatus.SUCCESS;
                var data = new UserResponseModel();

                oraConn = ORAConnection.Ins.DBConnection;
                oraConn.Open();
                if(user.M_USERNAME != null && user.M_PASSWORD != null)
                {
                    rescode = UserLogic.Ins.UserLoginLogic(oraConn, user, out data);
                }

                if(rescode == SystemStatus.SUCCESS)
                {
                    data.userDatas[0].TOKEN = AppSettings.Ins.GenerateToken(data.userDatas[0].M_USERNAME);
                }

                if (string.IsNullOrEmpty(message)) message = Messenger.Ins.GetMessage(rescode);
                output = new OutputModel(rescode,message,data);

                return output;

            }
            catch (Exception ex)
            {

                throw new Exception("UserLoginMng: "+ex.Message);
            }
        
        }
    }
}
